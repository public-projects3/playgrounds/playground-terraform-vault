FROM gitpod/workspace-full:2022-10-30-18-48-35
USER root
# Install Terraform
RUN wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | tee /usr/share/keyrings/hashicorp-archive-keyring.gpg && \
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/hashicorp.list && \
apt update -y && apt install terraform -y && \
# Install Vault
apt install vault -y && \
# Install AWS
pip install --upgrade pip && \
pip install --upgrade awscli
USER gitpod
