# Overview

This is a playground primarily for Vault and Terraform. It also includes the following:

- Docker
- AWS CLI
- Go
- Java
- Node.js
- C/C++
- Python
- Ruby
- Rust
- Clojure
- Homebrew
- Tailscale
- Nginx
- and several more

## Course Recommendations

It's recommended to use this image with the following courses:
- [Terraform 101 - Certified Terraform Associate](https://courses.tekanaid.com/p/terraform101)
- [HashiCorp Vault 101 - Certified Vault Associate](https://courses.tekanaid.com/p/hashicorp-vault-101-certified-vault-associate)